<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    protected $username;
    /**
     * @ORM\OneToMany(
     *      targetEntity="Post",
     *      mappedBy="for_user",
     *      orphanRemoval=true
     * )
     * @ORM\OrderBy({"createDate" = "DESC"})
     */
    private $posts;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        //$this->posts = array('ROLE_ADMIN');
    }

    public function getId(){
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }
    public function getPosts()
    {
        return $this->posts;
    }
}
