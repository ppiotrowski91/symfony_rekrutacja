-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 21 Lut 2016, 18:23
-- Wersja serwera: 10.1.9-MariaDB
-- Wersja PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `rekrutacja_blog`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `author` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `content` text NOT NULL,
  `id_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `comment`
--

INSERT INTO `comment` (`id`, `author`, `email`, `create_date`, `content`, `id_post`) VALUES
(1, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:04:43', 'dsasdadsadsasdadsasda', 3),
(2, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:07:05', 'sdadsadsasadsdadsa', 3),
(3, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:07:15', 'sdadssdassdasdadsasaddsasadadsasadsdadsa', 3),
(4, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:07:21', 'sdadssdassdasdadsasaddsasadadsasadsdsdasdasdasdadsa', 3),
(5, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:07:38', 'sdsdasad213231231231', 3),
(6, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:07:47', 'sdsdasad213213213213231231231231231', 3),
(7, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:11:28', 'dsadsadsadsasdadsa', 3),
(8, 'XXX', 'XXX@xxx.pl', '2016-02-17 23:11:34', 'dsadsadsadsasdadsa', 3),
(9, 'XXX', 'XXX@xxx.pl', '2016-02-18 21:37:52', 'sdasdasdasaddsadsadsadssdsdasdds', 3),
(10, 'XXX', 'XXX@xxx.pl', '2016-02-18 23:12:01', 'dssadsdadsasdadsa', 3),
(11, 'XXX', 'XXX@xxx.pl', '2016-02-19 00:11:14', 'sadadsadsdasdasadsadsd', 3),
(12, 'XXX', 'XXX@xxx.pl', '2016-02-19 00:11:49', 'sadadsadsdasdasadsadsd', 3),
(13, 'XXX', 'XXX@xxx.pl', '2016-02-19 22:09:56', 'saddsadsadsadsasaddas', 3),
(14, 'dsasda', 'saddasdsa@asdasd.com', '2016-02-19 22:18:29', 'saddsadsadsadsasaddas', 3),
(15, 'dsasdasadsad', 'saddasdsa@asdasd.com', '2016-02-19 22:24:49', 'saddsadsadsadsasaddas', 3),
(16, 'sdsdasadasd', 'dsasdadsa@assd.com', '2016-02-19 23:16:50', 'dssaddsadsaadsdsasdasadsdasad', 3),
(17, 'sdsdasadasd', 'dsasdadsa@asd.com', '2016-02-19 23:17:32', 'dssaddsadsaadsdsasdasadsdasad', 3),
(18, 'sdsdasadasd', 'dsasdadsa@asd.com', '2016-02-19 23:21:41', 'dssaddsadsaadsdsasdasadsdasad', 3),
(19, 'sdsdasadasdsdsdasadasdsdsdasadasdsdsdasadasdsdsdas', 'dsasdadsa@asd.com', '2016-02-19 23:24:05', 'dssdadasdsasda', 3),
(20, 'sdaadssda', 'sdasddsadsa@asdasd.com', '2016-02-21 15:42:59', 'dsasdadasddsadsa', 8);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(3, 'uzytkownik1', 'uzytkownik1', 'uzytkownik1@symfony.com', 'uzytkownik1@symfony.com', 1, '8becgk6sy1ogk8w8sg4ck0gogcoskks', '$2y$13$8becgk6sy1ogk8w8sg4cku5rNyd.WrPNBSYnR4nUcOtWLVLonLIRe', '2016-02-21 15:39:39', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL),
(4, 'uzytkownik2', 'uzytkownik2', 'uzytkownik2@symfony.com', 'uzytkownik2@symfony.com', 1, 'f31tkbhjqts804s8soog000w0s88c8g', '$2y$13$f31tkbhjqts804s8soog0uKoyB49KMy6394c5.RqevOejDQh8cV/S', '2016-02-21 14:01:32', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `publish_at` datetime NOT NULL,
  `id_fos_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `status`, `create_date`, `publish_at`, `id_fos_user`) VALUES
(1, 'dssadsda', 'dsasdasdadsadsadas', 1, '2016-02-15 20:26:43', '2016-02-15 20:26:43', 3),
(2, 'posted', 'sdadsasaddsadsa', 1, '2016-02-16 21:20:14', '2016-02-16 21:20:14', 3),
(3, 'dsasdadsasad', 'sadsdasdasadsdasdasdadsasadsaddsasad', 1, '2016-02-16 21:20:38', '2016-02-16 21:20:38', 3),
(4, 'saddssaddas', 'sdasaddsadsadsadsadsa', 1, '2016-02-21 00:08:20', '2016-02-21 00:08:20', 3),
(5, 'sdadsadsasadsda', 'sadsdasdasdasaddsa', 1, '2016-02-21 10:31:50', '2016-02-16 00:00:00', 3),
(6, 'Lorem ipsum dolor sit amet', 'Sed ac sem non justo dignissim blandit nec ac tortor. Praesent velit ipsum, pretium vel egestas eu, vestibulum eu ipsum. Nulla euismod egestas nisi vitae ullamcorper. In varius tortor eu magna elementum, egestas pretium ligula pretium. Phasellus lacus dui, pellentesque vel lectus a, semper aliquam tortor. Donec tincidunt dignissim quam, ac fringilla sem luctus rutrum. Ut interdum cursus viverra.', 3, '2016-02-21 10:50:07', '2016-02-24 00:00:00', 3),
(7, 'posted posted posteddddddd', 'sdasdasadsdasdasdasdsdsadsdasdasdasad', 2, '2016-02-21 14:02:59', '2016-02-23 00:00:00', 4),
(8, 'dssadsad', 'dsadassdasadsdasda', 1, '2016-02-21 14:10:25', '2016-02-21 14:10:25', 4);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
